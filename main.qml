import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    id: window
    // @disable-check M16
    width: Screen.width
    // @disable-check M16
    height: Screen.height
    // @disable-check M16
    visible: true
    // @disable-check M16
    title: qsTr("ACS_BluetoothSearch")

    Connections{
        target: bluetoothClassObj
        function onSignalToUpdateDataStringInQml(qstrData){
            messageTransmitValue.text = qstrData;
        }
    }

    Text {
        id: messageTransmitLabel
        y: 52
        height: 36
        anchors.left: parent.left
        anchors.right: parent.right
        font.pixelSize: 20
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.styleName: "Bold"
        minimumPixelSize: 12
        anchors.rightMargin: 34
        anchors.leftMargin: 50
        text: qsTr("Message being Transmitted : ")
    }

    Text {
        id: messageTransmitValue
        y: 102
        height: 36
        anchors.left: parent.left
        anchors.right: parent.right
        font.pixelSize: 20
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        font.styleName: "Bold"
        minimumPixelSize: 12
        anchors.rightMargin: 34
        anchors.leftMargin: 50
        text: qsTr("Transmitting data. . . ")
    }
    //    property string deviceName: ""
    //    property string deviceAddress: ""
    //    property int iCount: 0
    //    Connections{
    //        target: bluetoothClassObj
    //        function onSignalToUpdateBtDevicesInQml(qstrDeviceName,qstrDeviceAddress){
    //            console.log("signal received in qml")
    //            if(iCount == 0){
    //                searchModel.set(0,{name: qstrDeviceName, address: qstrDeviceAddress});
    //            } else {
    //                searchModel.append({name: qstrDeviceName, address: qstrDeviceAddress});
    //            }
    //            iCount++;
    //        }

    //        function onSignalToResetCounterAndSetSearchComplete(){
    //            iCount = 0;
    //            searchLabel.text = "Search Complete"
    //        }
    //    }

    //    ListView {
    //        id: view
    //        height: 600
    //        width: 300
    //        anchors.centerIn: parent
    //        focus: true
    //        spacing: 4
    //        cacheBuffer: 50
    //        model: searchModel
    //        delegate: Component{
    //            Item {
    //                id: delegateItem
    //                width: view.width
    //                height: view.height/8
    //                scale: state === "Pressed" ? 0.96 : 1.0
    //                Behavior on scale {
    //                    NumberAnimation {
    //                        duration: 100
    //                        easing.type: Easing.InOutQuad
    //                    }
    //                }
    //                Rectangle{
    //                    id: delegateItemRectangle
    //                    height: delegateItem.height
    //                    width: delegateItem.width
    ////                    anchors.fill: parent
    //                    border.color: "#ffffff"
    //                    border.width: 2
    //                    radius: 2
    //                    color: "#C1ECF1"
    //                    Column{
    //                        spacing: 0

    //                        Text{text: "Machine Name : " + name}
    //                        Text{text: "Address : " + address}
    //                    }
    //                }
    //                states:[
    //                    State {
    //                        name: "Pressed"
    //                        PropertyChanges {
    //                            target: delegateItemRectangle
    //                            color: "#016362"
    //                        }
    //                    }
    //                ]
    //                transitions: [
    //                    Transition {
    //                        from: "*"; to: "Pressed"
    //                        ColorAnimation {duration: 10}
    //                    }
    //                ]
    //                MouseArea{
    //                    anchors.fill: parent
    //                    onClicked: {
    //                        bluetoothClassObj.createBluetoothVirtualServer(index);
    //                    }
    //                    onPressed: {delegateItem.state = "Pressed"}
    //                    onReleased: {delegateItem.state = ""}
    //                }
    //            }
    //        }
    //    }

    //    ListModel {
    //        id: searchModel
    //        ListElement {
    //            name: "0"
    //            address: "0"
    //        }
    //    }

    //    Text {
    //        id: searchLabel
    //        y: 52
    //        height: 36
    //        text: qsTr("Searching. . . .")
    //        anchors.left: parent.left
    //        anchors.right: parent.right
    //        font.pixelSize: 20
    //        horizontalAlignment: Text.AlignHCenter
    //        verticalAlignment: Text.AlignVCenter
    //        font.styleName: "Bold"
    //        minimumPixelSize: 12
    //        anchors.rightMargin: 34
    //        anchors.leftMargin: 50
    //    }
}

/*##^##
Designer {
    D{i:0;formeditorZoom:2}D{i:11}
}
##^##*/
