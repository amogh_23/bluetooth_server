#ifndef BLUETOOTH_H
#define BLUETOOTH_H

#include <QObject>
#include <QDebug>
#include <QBluetoothLocalDevice>
#include <QBluetoothDeviceInfo>
#include <QBluetoothServer>
#include <QBluetoothUuid>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QTimer>

const int MAX = 26;
class bluetooth : public QObject
{
    Q_OBJECT
public:
    explicit bluetooth(QObject *parent = nullptr);
    ~bluetooth();
    void searchBluetoothDevices();
    void startDeviceDiscovery();
    void localDeviceStatus();

    void stopServer();
    Q_INVOKABLE void createBluetoothVirtualServer();

public slots:
    void transmitData(const QString &data);
    void clientConnected();

signals:
    void signalToUpdateBtDevicesInQml(QString qstrDeviceName, QString qstrDeviceAddress);
    void signalToResetCounterAndSetSearchComplete();
    void messageReceived(const QString &sender, const QString &message);
    void clientConnected(const QString &name);
    void signalToUpdateDataStringInQml(QString qstrData);

private slots:
    void devicesDiscovered();

    void clientDisconnected();
    void readSocket();
    void generateRandomString();

private:
    QBluetoothDeviceDiscoveryAgent *discoveryAgent;
    QBluetoothLocalDevice localDevice;
    QBluetoothServer *rfcommServer = nullptr;
    QBluetoothServiceInfo serviceInfo;
    QList<QBluetoothSocket *> clientSockets;
    QTimer *timer;
};

#endif // BLUETOOTH_H
