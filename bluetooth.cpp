#include "bluetooth.h"
//! [Service UUID]
static const QLatin1String serviceUuid("e8e10f95-1a70-4b27-9ccf-02010264e9c8");
//! [Service UUID]
bluetooth::bluetooth(QObject *parent) : QObject(parent)
{

}

bluetooth::~bluetooth()
{
    stopServer();
}

void bluetooth::searchBluetoothDevices()
{
    QBluetoothLocalDevice localBtDevice;
    QBluetoothAddress deviceBTMacAddress = localBtDevice.address();
    QString deviceBTName = localBtDevice.name();
}

void bluetooth::startDeviceDiscovery()
{
    discoveryAgent = new QBluetoothDeviceDiscoveryAgent(this);
    connect(discoveryAgent, SIGNAL(finished()), this, SLOT(devicesDiscovered()));
    discoveryAgent->start();                    // Start a discovery
}

void bluetooth::localDeviceStatus()
{
    QString localDeviceName;
    if (localDevice.isValid()) {                // Check if Bluetooth is available on this device
        localDevice.powerOn();                  // Turn Bluetooth on
        localDeviceName = localDevice.name();   // Read local device name
        qDebug() << "LocalDeviceName : " << localDeviceName;
        qDebug() << "LocalDeviceAddress : " << localDevice.address().toString();
        localDevice.setHostMode(QBluetoothLocalDevice::HostDiscoverable);
        //        startDeviceDiscovery();                 //startBluetoothScan
        createBluetoothVirtualServer();
    }
}

void bluetooth::stopServer()
{
    serviceInfo.unregisterService();    // Unregister service
    qDeleteAll(clientSockets);          // Close sockets
    serviceInfo.unregisterService();
    rfcommServer->close();
    delete rfcommServer;                // Close server
    rfcommServer = nullptr;
//    timer->stop();
}

void bluetooth::createBluetoothVirtualServer()
{
    //    QList<QBluetoothDeviceInfo> devices = discoveryAgent->discoveredDevices();
    qDebug() << "Creating virtual server";
    const QBluetoothAddress &localBTAdapterAddress = localDevice.address();
    rfcommServer = new QBluetoothServer(QBluetoothServiceInfo::RfcommProtocol,this);
    connect(rfcommServer, &QBluetoothServer::newConnection, this, QOverload<>::of(&bluetooth::clientConnected));
    bool xResult = rfcommServer->listen(localBTAdapterAddress);
    if(!xResult){
        qDebug() << "Cannot bind to BT server " << localBTAdapterAddress.toString();
        return;
    }

    QBluetoothServiceInfo::Sequence profileSequence;
    QBluetoothServiceInfo::Sequence classId;
    classId << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::SerialPort));
    classId << QVariant::fromValue(quint16(0x100));
    profileSequence.append(QVariant::fromValue(classId));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BluetoothProfileDescriptorList,
                             profileSequence);

    classId.clear();
    classId << QVariant::fromValue(QBluetoothUuid(serviceUuid));
    classId << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::SerialPort));

    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceClassIds, classId);

    //! [Service name, description and provider]
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceName, tr("ACService"));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceDescription,
                             tr("Get status from AC service machine"));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ServiceProvider, tr("Nexion"));
    //! [Service name, description and provider]

    //! [Service UUID set]
    serviceInfo.setServiceUuid(QBluetoothUuid(serviceUuid));
    //! [Service UUID set]

    //! [Service Discoverability]
    QBluetoothServiceInfo::Sequence publicBrowse;
    publicBrowse << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::PublicBrowseGroup));
    serviceInfo.setAttribute(QBluetoothServiceInfo::BrowseGroupList,
                             publicBrowse);
    //! [Service Discoverability]

    //! [Protocol descriptor list]
    QBluetoothServiceInfo::Sequence protocolDescriptorList;
    QBluetoothServiceInfo::Sequence protocol;
    //    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::L2cap));
    //    protocolDescriptorList.append(QVariant::fromValue(protocol));
    protocol.clear();
    protocol << QVariant::fromValue(QBluetoothUuid(QBluetoothUuid::Rfcomm))
             << QVariant::fromValue(quint8(rfcommServer->serverPort()));
    protocolDescriptorList.append(QVariant::fromValue(protocol));
    serviceInfo.setAttribute(QBluetoothServiceInfo::ProtocolDescriptorList,
                             protocolDescriptorList);
    //! [Protocol descriptor list]

    bool xPublishServiceResult = serviceInfo.registerService();
    if(xPublishServiceResult){
        qDebug() << "Service successfully published on local BT adapter";
    } else {
        qDebug() << "Failed to publish service";
    }
    qDebug() << "Virtual server created";
}

void bluetooth::transmitData(const QString &data)
{
    QByteArray text = data.toUtf8() + '\n';

    for (QBluetoothSocket *socket : qAsConst(clientSockets))
        socket->write(text);
}

void bluetooth::devicesDiscovered()
{
    QList<QBluetoothDeviceInfo> devices = discoveryAgent->discoveredDevices();
    if(devices.isEmpty())    qDebug() << "No Bluetooth Devices Found";
    int listSize = devices.size();
    qDebug() << listSize << "Devices Found!!";
    for(int i=0; i<listSize; i++){
        QString deviceName = devices[i].name();
        QString deviceAddress = devices[i].address().toString();
        qDebug() << "Device Name : (" << i << ")" << " : " << deviceName;
        qDebug() << "Device Address : (" << i << ")" << " : " << deviceAddress;
        emit signalToUpdateBtDevicesInQml(deviceName,deviceAddress);
    }
    emit signalToResetCounterAndSetSearchComplete();
}

void bluetooth::clientConnected()
{
    QBluetoothSocket *socket = rfcommServer->nextPendingConnection();
    if (!socket)    return;
    connect(socket, &QBluetoothSocket::readyRead, this, &bluetooth::readSocket);
    connect(socket, &QBluetoothSocket::disconnected, this, QOverload<>::of(&bluetooth::clientDisconnected));
    clientSockets.append(socket);
    qDebug() << socket->peerName() << "connected";
    qDebug() << "Start transmitting data . . . .";
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(generateRandomString()));
    timer->start(2000);
    //    emit clientConnected(socket->peerName());
}

void bluetooth::clientDisconnected()
{

    QBluetoothSocket *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)    return;
    clientSockets.removeOne(socket);
    socket->deleteLater();
    qDebug() << socket->peerName() << "disconnected";
}

void bluetooth::readSocket()
{
    QBluetoothSocket *socket = qobject_cast<QBluetoothSocket *>(sender());
    if (!socket)    return;
    while (socket->canReadLine()) {
        QByteArray line = socket->readLine().trimmed();
        //        emit messageReceived(socket->peerName(),QString::fromUtf8(line.constData(), line.length()));
        qDebug() << "Message received from " << socket->peerName() << "is" <<
                    QString::fromUtf8(line.constData(), line.length());
    }
}

void bluetooth::generateRandomString()
{
    const int n=10;
    char alphabet[MAX] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
                           'h', 'i', 'j', 'k', 'l', 'm', 'n',
                           'o', 'p', 'q', 'r', 's', 't', 'u',
                           'v', 'w', 'x', 'y', 'z' };

    QString qstrRandomData = "";
    for (int i = 0; i < n; i++)
        qstrRandomData = qstrRandomData + alphabet[rand() % MAX];

    transmitData(qstrRandomData);
    emit signalToUpdateDataStringInQml(qstrRandomData);
}
